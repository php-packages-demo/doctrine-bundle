# doctrine-bundle

[**doctrine/doctrine-bundle**](https://packagist.org/packages/doctrine/doctrine-bundle)
![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-bundle)
Doctrine DBAL & ORM Bundle for Symfony https://www.doctrine-project.org/projects/doctrine-bundle.html

* [*Databases and the Doctrine ORM*](https://symfony.com/doc/current/doctrine.html)
* Symfony5: The Fast Track
  * [*Step 7: Setting up a Database*
    ](https://symfony.com/doc/current/the-fast-track/en/7-database.html)
  * [*Step 8: Describing the Data Structure*
    ](https://symfony.com/doc/current/the-fast-track/en/8-doctrine.html)
  * Step 11: Branching the Code [*Storing Sessions in the Database*
    ](https://symfony.com/doc/current/the-fast-track/en/11-branch.html#storing-sessions-in-the-database)
* [*Association Mapping*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/association-mapping.html)
---
* [*Ignore custom indexes on Doctrine DBAL 4*
  ](https://dev.to/indragunawan/ignore-custom-indexes-on-doctrine-dbal-4-387j)
  2024-07 Indra Gunawan (DEV)
* [*Custom DQL usage with Symfony*
  ](https://medium.com/beyn-technology/custom-dql-usage-with-symfony-a62965154172)
  2023-09 Oğuzhan KARACABAY @ Medium
* [*How to create a custom DQL function in Symfony*
  ](https://medium.com/simform-engineering/how-to-create-custom-dql-function-symfony-84da7044c714)
  2023-02 Bhavin Nakrani
